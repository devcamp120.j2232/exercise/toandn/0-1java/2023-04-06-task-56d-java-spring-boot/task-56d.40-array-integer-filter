package com.devcamp.arrayintegerfilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ArrayIntegerFilterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArrayIntegerFilterApplication.class, args);
	}

}
